var path          = require('path');

// PostCSS
var atImport      = require('postcss-import');
var inlineSvg     = require('postcss-inline-svg');
var mixins        = require('postcss-mixins');
var simpleVars    = require('postcss-simple-vars');
var conditionals  = require('postcss-conditionals');
var colorFunction = require('postcss-color-function');
var calc          = require('postcss-calc');
var nested        = require('postcss-nested');
var autoprefixer  = require('autoprefixer');


var getPostcssStack = function(webpack) {
  return [

    //parse @import statements
    atImport({
      path: [
        path.resolve(__dirname, './node_modules'),
        path.resolve(__dirname, './css')
      ],
    }),

    inlineSvg({
      path: path.resolve(__dirname, './img')
    }),

    mixins(),

    //parse $var: 'some-awesomeo-variable';
    simpleVars({
      variables: function() {
        return require('./config/variables.js');
      }
    }),

    conditionals(),

    colorFunction(),

    //parse .className{ &:hover{} }
    nested(),

    //parse calc();
    calc(),

    //make old browsers work like a boss (kinda...)
    autoprefixer()
  ];
};


module.exports = getPostcssStack;
