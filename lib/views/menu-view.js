import Backbone   from 'backbone';
import Marionette from 'backbone.marionette';
import styles     from '../../css/pages/menu.css';

import itemTemplate from '../../templates/menu/item.hbs';

//Collection
const menuCollection = new Backbone.Collection([
  { name: 'Headings', url: '#headings' },
  { name: 'Buttons', url: '#buttons' },
  { name: 'Links', url: '#links' },
  { name: 'Action Elements', url: '#action-elements' },
  { name: 'Tooltips', url: '#tooltips' },
  { name: 'Dropdowns', url: '#dropdowns' },
  //{ name: 'Tabs', url: '#tabs' },
  { name: 'Modals', url: '#modals' },
  { name: 'Patterns', url: '#patterns' },
]);

//Item View
const MenuItemView = Marionette.ItemView.extend({
  tagName:   'li',
  className: 'menu__item',
  template:  itemTemplate,
});

//Collection View
export const MenuView = Marionette.CollectionView.extend({
  childView:  MenuItemView,
  collection: menuCollection,
});
