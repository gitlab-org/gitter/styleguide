import Marionette from 'backbone.marionette';
import styles     from '../../css/pages/headings.css';
import template   from '../../templates/headings/layout.hbs';

export const HeadingsLayout = Marionette.LayoutView.extend({
  template: template
});
