import Marionette   from 'backbone.marionette';
import styles       from '../../css/pages/links.css';
import template     from '../../templates/links/layout.hbs';

export const LinksLayout = Marionette.LayoutView.extend({
  template: template
});
