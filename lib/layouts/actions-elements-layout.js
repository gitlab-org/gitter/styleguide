import Marionette from 'backbone.marionette';
import styles     from '../../css/pages/action-elements.css';
import template   from '../../templates/action-elements/layout.hbs';


console.log('-----------------------');
console.log(styles);
console.log('-----------------------');

export const ActionElementsLayout = Marionette.LayoutView.extend({
  template:  template,
});
