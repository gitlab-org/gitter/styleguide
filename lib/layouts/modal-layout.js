import Marionette from 'backbone.marionette';
import styles     from '../../css/pages/modals.css';
import template   from '../../templates/modals/layout.hbs';

export const ModalsLayout = Marionette.LayoutView.extend({
  template: template
});
